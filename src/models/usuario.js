const mongoose = require("mongoose");
const uniqueValidator = require('mongoose-unique-validator');


let Schema = mongoose.Schema;

let roles = {
    values: ['ADMIN_ROLE','PROF_ROLE','USER_ROLE'],
    message: '{VALUE} no es un rol válido'
};

let usuarioSchema = new Schema(
    {
        nombre: {
            type: String,
            required: [true, 'El nombre es necesario']
        },
        email: {
            type: String,
            required: [true, 'El correo es necesario'],
            unique: true
        },
        pass: {
            type: String,
            required: [true, 'La contraseña es necesaria']
        },
        role: {
            type: String,
            default: "USER_ROLE",
            enum: roles
        },
        google: {
            type: Boolean,
            default: false
        }
    }
);

usuarioSchema.methods.toJSON = function(){
    let user = this;
    let userObject = user.toObject();
    delete userObject.pass;

    return userObject;
}

usuarioSchema.plugin(uniqueValidator, {message: '{PATH} debe de ser único'});

var Usuario = module.exports = mongoose.model('usuario', usuarioSchema);