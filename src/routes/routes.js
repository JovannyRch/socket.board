const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const _ = require('underscore');
const path = require('path');
const jwt = require('jsonwebtoken');
const {jwtConfig} = require('../config')
const {verificarToken} = require('../middlewares/autentificacion');
Usuario = require('../models/usuario');





router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(bodyParser.json());


// Rutas HTML

router.get('/login', function (req, res) {
    res.sendFile(path.join(__dirname, '../public/login.html'));
});

router.get('/registro', function (req, res) {
    res.sendFile(path.join(__dirname, '../public/signup.html'));
});

router.get('/alumno', function (req, res) {
    res.sendFile(path.join(__dirname, '../public/dashAlumno.html'));
});

router.get('/docente', function (req, res) {
    res.sendFile(path.join(__dirname, '../public/dashMaestro.html'));
});

router.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '../public/index.html'));
});


router.get('/clase', function (req, res) {
    res.sendFile(path.join(__dirname, '../public/alumno.html'));
});


router.get('/clase/docente', function (req, res) {
    res.sendFile(path.join(__dirname, '../public/boardProfe.html'));
});






function returnError(res, err) {
    return res.status(400).json({
        ok: false,
        mensaje: err
    })
}

function returnResponse(res, response) {
    return res.json({
        ok: true,
        response
    })
}

// RUTAS API REST

router.post('/api/login', (req, res, next) => {
    let body = req.body;
    let email = body.email;
    var pass = body.pass;


    Usuario.findOne({ email: email }, (err, usuarioDB) => {
        
        if(err == null && usuarioDB == null) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            })
        }

        if (err) {
            returnError(res, err);
        }
        if (!usuarioDB) {
            returnError(res, "Datos no encontrados");
        }

        if (!bcrypt.compareSync(pass, usuarioDB.pass)) {
            returnError(res, "Datos no encontrados");
        }

        let token = jwt.sign(
            {
                usuario: usuarioDB
            },
            jwtConfig.seed,
            {
                expiresIn: jwtConfig.fecha
            }
        );

        return res.json({
            ok: true,
            usuario: usuarioDB,
            token
        })
    });
});


router.post('/api/signup', (req, res, next) => {
    let body = req.body;


    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        pass: bcrypt.hashSync(body.pass, 10),
    });

    usuario.save((err, usuarioDB) => {
        
        if (usuarioDB == undefined || usuarioDB == null) {
            
            return res.status(400).json({
                ok: false,
                mensaje: err
            })
        }

        //Guardar sesión el de alumno    
        let token = jwt.sign(
            {
                usuario: usuarioDB
            },
            jwtConfig.seed,
            {
                expiresIn: jwtConfig.fecha
            }
        );


        res.json({
            ok: true,
            usuario: usuarioDB,
            token
        });

    });
})




router.post('/api/usuario', (req, res, next) => {
    let body = req.body;


    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        pass: bcrypt.hashSync(body.pass, 10),
    });

    usuario.save((err, usuarioDB) => {
        if (err) {
            returnError(res, err);
        }

        let token = jwt.sign(
            {
                usuario: usuarioDB
            },
            jwtConfig.seed,
            {
                expiresIn: jwtConfig.fecha
            }
        );


        res.status(201).json({
            ok: true,
            usuario: usuarioDB,
            token
        })
    });
}).put('/api/usuario/:id', (req, res, next) => {
    let id = req.params.id;
    let body = _.pick(req.body, ['nombre', 'role', 'pass']);


    Usuario.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, usuarioDB) => {
        if (err) {
            returnError(res, err);
        }

        res.json({
            ok: true,
            usuario: usuarioDB
        })

    });

}).get('/api/usuario/:id', function (req, res) {

    let id = req.params.id;
    if (id) {
        Usuario.findById(id)
            .exec((err, usuario) => {
                if (err) {
                    returnError(res, err);
                }

                res.json({
                    ok: true,
                    usuario
                })
            });
    }
}).get('/api/usuarios',verificarToken, function (req, res) {
    let desde = req.query.desde || 0;
    desde = Number(desde);


    let limite = req.query.limite || 10;
    limite = Number(limite);

    total = 0;

    Usuario.count({}, (err, conteo) => {
        total = conteo;
    });

    Usuario.find({})
        .skip(desde)
        .limit(limite)
        .exec((err, usuarios) => {
            if (err) {
                returnError(res, err);
            }

            res.json({
                ok: true,
                usuarios,
                total: total
            })
        });
});




module.exports = router;