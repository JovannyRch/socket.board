const jwt = require('jsonwebtoken');
const {jwtConfig} = require('../config');

//Verificar Token

let verificarToken = (req,res,next) => {
    let token = req.get('token');
    jwt.verify(token,jwtConfig.seed, (err,decoded) => {
        if(err){
            return res.status(401).json(
                {
                    ok: false,
                    err
                }
            )
        }

        req.usuario = decoded.usuario;
        next();

    })
};

module.exports = {
    verificarToken
}