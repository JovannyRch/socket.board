const socket = io();

var app = new Vue(
    {
        el: "#app",
        data: {
            saludo: "hola",
            usuario: "JovannyRch",
            mensaje: "",
            mensajes: [],
            sizeCtx: 7,
            colorPen: ''
        },
        methods: {
            newMsg() {
                if (this.mensaje) {
                    socket.emit("newMsg", { mensaje: this.mensaje, usuario: this.usuario });
                    this.mensaje = "";
                }
            },
            onNewMsg(mensaje) {
                this.mensajes.unshift(mensaje)
            }
        }
    }
)


socket.on("newMsg", (mensaje) => {
    app.onNewMsg(mensaje);
});



var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');


var painting = document.getElementById('board');
var paint_style = getComputedStyle(painting);
canvas.width = parseInt(paint_style.getPropertyValue('width'));
canvas.height = parseInt(paint_style.getPropertyValue('height'));

var mouse = { x: 0, y: 0 };
ctx.lineWidth = 11;
ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.strokeStyle = '#00CC99';

function mapear(x, y, mW, mH) {
    let resultado = { x: 0, y: 0 };
    resultado.x = x * canvas.width / mW;
    resultado.y = y * canvas.height / mH;
    return resultado;
}


socket.on('alum:mousemove', (data) => {
    if (data.pintando) {
        ctx.lineTo(mouse.x, mouse.y);
        ctx.stroke();
    } else {
        ctx.moveTo(mouse.x, mouse.y);
    }

    let coords = mapear(data.mouse.x, data.mouse.y, data.width, data.height);
    mouse.y = coords.y;
    mouse.x = coords.x;

});



socket.on('alum:changeColor', (data) => {
    ctx.strokeStyle = data;
    ctx.beginPath();
});



socket.on("setWidth", (width) => {
    ctx.lineWidth = width;
});