const socket = io();


var app = new Vue({
    el: "#app",
    data:{
        mensaje: '',
        usuario: 'Profe 1',
        mensajes: [],
        lineWidth: 17,
        colorPen: '#000000'

    },
    methods:{
        onNewMsg(mensaje){
            this.mensajes.unshift(mensaje)
        },
        newMsg(){
            if(this.mensaje){
                socket.emit("newMsg",{mensaje: this.mensaje, usuario: this.usuario});
                this.mensaje = "";
            }
        },
        setSize(){
            ctx.lineWidth = this.lineWidth;
            socket.emit("setWidth",this.lineWidth);
        },
        setColor(color){
            this.colorPen = color;
            ctx.strokeStyle = color;
            socket.emit("profe:changeColor", {color});
        },
        changeColor(){
            this.setColor(this.colorPen);
        }
    }
})


socket.on("newMsg", (mensaje) => {
    app.onNewMsg(mensaje);
});

var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

var painting = document.getElementById('board');
var paint_style = getComputedStyle(painting);
canvas.width = parseInt(paint_style.getPropertyValue('width'));
canvas.height = parseInt(paint_style.getPropertyValue('height'));

var mouse = { x: 0, y: 0 };

let pintando = false;



canvas.addEventListener('mousemove', function (e) {
    mouse.x = e.pageX - this.offsetLeft;
    mouse.y = e.pageY - this.offsetTop;

    socket.emit("profe:mousemove", {mouse: mouse, pintando: pintando, height: canvas.height, width: canvas.width});
}, false);

ctx.lineWidth = 11;
ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.strokeStyle = '#00CC99';

canvas.addEventListener('mousedown', function (e) {
    pintando = true;    
    ctx.beginPath();
    ctx.moveTo(mouse.x, mouse.y);
    canvas.addEventListener('mousemove', onPaint, false);
}, false);

canvas.addEventListener('mouseup', function () {
    pintando = false;    
    canvas.removeEventListener('mousemove', onPaint, false);
}, false);

var onPaint = function () {
    ctx.lineTo(mouse.x, mouse.y);
    ctx.stroke();
};




