const express = require("express");
const path = require('path');
const morgan = require("morgan");

require('./database');
const app = express();

// Static files
app.set("views",path.join(__dirname, "views"));

//Middlewares
app.use(morgan('dev'));


//Public
app.use(express.static(path.resolve(__dirname,"public")));


//Rutas

app.use('/', require('./routes/routes'));


// Starting server
app.set('port', process.env.PORT || 3000);

const server = app.listen(app.get("port"), () => {
    console.log("Hola :D");
})




// Socket.io
const socket = require('socket.io');
const io = socket(server);


io.on('connection', (socket) => {

    // Evento mover el mouse
    socket.on('profe:mousemove', (data) => {
        io.sockets.emit('alum:mousemove', data);
    })

    //Evento cambiar el color
    socket.on('profe:changeColor', (color) => {
        io.sockets.emit('alum:changeColor', color.color);
    });

    //Evento nuevo mensaje
    socket.on('newMsg', (data) => {
        //io.sockets.emit('profe:mensaje', mensaje);
        io.sockets.emit('newMsg', data);
        
    });

    //Evento cambiar de tamaño del lapiz
    socket.on("setWidth", (width)=> {
        io.sockets.emit('setWidth', width);
    });


});


