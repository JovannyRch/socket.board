const mongoose = require("mongoose");
const { mongodb } = require('./config');


mongoose.connect(mongodb.URI, {useNewUrlParser: true}).then( 
    db => {
        console.log("Conexión con la base de datos");
    }
).catch(
    err => {
        console.log("Error al conectar");
    }
)